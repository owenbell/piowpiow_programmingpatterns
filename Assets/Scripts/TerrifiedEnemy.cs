﻿using UnityEngine;
using System.Collections;

namespace gc {

public class TerrifiedEnemy : Enemy {


	protected override void Awake() {
		base.Awake();

		mSpawnClip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/Scream1");
		mDestroyClip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/Nom2");
	}

	// Use this for initialization
	void Start () {
		base.Start();
	}

	// Update is called once per frame
	void Update () {
		base.Update();
	}
}

}
