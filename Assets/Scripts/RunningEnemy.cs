﻿using UnityEngine;
using System.Collections;

namespace gc {

public class RunningEnemy : Enemy {

	protected override void Awake() {
		base.Awake();

		mSpawnClip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/Run");
		mDestroyClip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/Nom1");
	}

	// Use this for initialization
	void Start () {
		base.Start();
	}

	// Update is called once per frame
	void Update () {
		base.Update();
	}
}

}
