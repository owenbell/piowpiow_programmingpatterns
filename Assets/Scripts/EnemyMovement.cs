﻿
using UnityEngine;

namespace gc {

    public class EnemyMovement : MovementController {

		public float MaxSpeed;

        protected override void FixedUpdate()
        {
            Vector2 directionToPlayer = DirectionToPlayer();
            MoveInDirectionAtSpeed(-directionToPlayer, MaxSpeed);
        }

        protected Vector2 DirectionToPlayer() {
            Player player = Player.Instance;
            if (player == null) {
                return Vector2.zero;
            }

            Vector2 playerPosition = player.transform.position;
            Vector2 myPosition = gameObject.transform.position;

            Vector2 offsetToPlayer = playerPosition - myPosition;
            Vector2 directionToPlayer = offsetToPlayer.normalized;
            return directionToPlayer;
        }

        protected float DistanceToPlayer() {
            Player player = Player.Instance;
            if (player == null) {
                return 0f;
            }

            float distance = Vector3.Distance(player.transform.position, gameObject.transform.position);
            return distance;
        }

        protected void MoveInDirectionAtSpeed(Vector2 direction, float speed) {
			Vector2 desiredVelocity = direction * speed;
			Vector2 steeringForce = desiredVelocity - body.velocity;
			Vector2.ClampMagnitude(steeringForce, speed);
            body.AddForce(steeringForce);
			float currentSpeed = body.velocity.magnitude;
			if (currentSpeed > MaxSpeed) {
                body.velocity = Vector2.ClampMagnitude(body.velocity, MaxSpeed);
			}

			float angle = Mathf.Rad2Deg * (Mathf.Atan2(body.velocity.y, body.velocity.x)) + 180; // The extra 180° are because the enemy assets are pointing to the left
			body.MoveRotation(angle);
        }

        protected void Freeze() {
            body.velocity = Vector2.zero;
        }
    }


}
