﻿using UnityEngine;
using UnityEngine.Assertions;

namespace gc {

	public class Enemy : MonoBehaviour {

		public static int NumEnemies { get; private set; }

		public int Health;

		public int Damage;

		protected AudioClip mSpawnClip;
		protected AudioClip mDestroyClip;

		protected virtual void Awake() {
			NumEnemies++;
		}

		protected virtual void Start() {
			AudioClip clip = mSpawnClip;
			if (mSpawnClip == null) {
				clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_appear");
			}
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
		}

		protected virtual void Update() {
			if (Health <= 0) {
				Destroy(gameObject);
			}
		}

		protected virtual void OnCollisionEnter2D(Collision2D coll) {
			if (coll.gameObject.CompareTag("PLAYER")) {
				this.ApplyDamage(1);
			}
		}

		void ApplyDamage(int damage) {
			Health -= damage;
			if (Health <= 0) {
				AudioClip clip = mDestroyClip;
				if (mDestroyClip == null) {
					clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_destroyed_1");
				}
				AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
			}
		}

		protected virtual void OnDestroy() {
			NumEnemies--;
		}

		void PlaySound(string path) {
			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource(path);
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
		}

		void SetDestroySound(string path) {
			mDestroyClip = (AudioClip)ResourceLoader.Instance.GetResource(path);
		}
	}

}
