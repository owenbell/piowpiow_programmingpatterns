using UnityEngine;

namespace gc {

    public class RunAwayMovement : EnemyMovement {

        protected override void FixedUpdate() {
            Vector2 directionToPlayer = -DirectionToPlayer();
            MoveInDirectionAtSpeed(directionToPlayer, MaxSpeed);
        }

    }

}
