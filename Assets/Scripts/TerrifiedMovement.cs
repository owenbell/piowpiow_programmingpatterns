using UnityEngine;

namespace gc {

    public class TerrifiedMovement : EnemyMovement {

        public float sprintDuration = 0.5f;
        public float shakeDuration = 0.5f;
        public float scareRange = 10f;
        public float shakeForce = 10f;

        public Sprite movingSprite;
        private Sprite mStandingSprite;

        private float mCurrentSprint = 0f;
        private float mCurrentShake = 0f;
        private Vector2 mSprintDirection;

        private int mShakeDirection = -1;

        void Start() {
            mStandingSprite = GetComponent<SpriteRenderer>().sprite;
        }

        protected override void FixedUpdate() {
            /*
            * primitive state machine for switching between the sprinting and
            * shaking states.
            */
            if (mCurrentSprint > 0f) {
                SprintUpdate();
            } else if (mCurrentShake > 0f) {
                ShakeUpdate();
            } else if (DistanceToPlayer() <= scareRange) {
                mCurrentSprint = sprintDuration;
                mSprintDirection = -DirectionToPlayer();
                GetComponent<SpriteRenderer>().sprite = movingSprite;
            }
        }

        private void SprintUpdate() {
            mCurrentSprint -= Time.deltaTime;
            // Vector2 directionToPlayer = DirectionToPlayer();
            MoveInDirectionAtSpeed(mSprintDirection, MaxSpeed);
            if (mCurrentSprint <= 0f) {
                // body.velocity = Vector2.zero;
                Freeze();
                mCurrentShake = shakeDuration;
                GetComponent<SpriteRenderer>().sprite = mStandingSprite;
            }
        }

        private void ShakeUpdate() {
            mCurrentShake -= Time.deltaTime;
            Vector2 direction = new Vector2(mShakeDirection, 0f);
            MoveInDirectionAtSpeed(direction, shakeForce);
            mShakeDirection *= -1;
            if (mCurrentShake <= 0f) {
                Freeze();
            }
        }

    }
}
